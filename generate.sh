#!/bin/bash

cd http-gen && python main.py \
  --proto-path ../proto \
  --py-server-flask ../back/webapi/main.py \
  --ts-path ../front/src/store/


#python -m grpc_tools.protoc \
#    -I proto \
#    --python_out=back/webapi/generated \
#    --grpc_python_out=back/webapi/generated \
#    proto/test.proto
#!/bin/bash

if [ ! -d alembic_pyenv ]; then
  echo -n "Installing alembic env..."
  python -m venv alembic_pyenv
  source alembic_pyenv/bin/activate
  pip install -r db/alembic-requirements.txt
  echo "OK"
fi;

echo -n "Activation alembic_pyenv..."
source alembic_pyenv/bin/activate
echo "OK"

echo "Starting DB revision..."
if [[ "$#" != 1 ]]; then
  echo "FAIL"
  echo "You need exactly one argument - db shema update message"
  exit 1
fi;

export $(grep -v '^#' $PWD/../.env.dev | xargs)
export POSTGRES_DB=${POSTGRES_DB_DEV}

alembic revision --autogenerate -m "$1"

alembic upgrade head

deactivate
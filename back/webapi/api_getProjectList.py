from sqlalchemy import select
from sqlalchemy.dialects.postgresql import array_agg
from sqlalchemy.engine import LegacyCursorResult
from typing import Tuple, Union
import webapi.generated_messages as msg
from db.tables import ProjectDB, ProjectPermission
from db.utils import Engine


def getProjectList(user_id: int) -> Tuple[Union[msg.ProjectList, str], int]:
    """
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.ProjectList | string - msg.ProjectList if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        select(
            ProjectDB.c.project_id, ProjectDB.c.title, ProjectDB.c.color,
            ProjectDB.c.section_list, ProjectDB.c.is_archived,
            array_agg(ProjectPermission.c.user_id),
        )
        .where(ProjectPermission.c.user_id == user_id, ProjectPermission.c.project_id == ProjectDB.c.project_id)
        .group_by(ProjectDB.c.project_id)
    )

    return msg.ProjectList(list=[
        msg.Project(
            project_id=project_id,
            title=title,
            color=color,
            section_list=section_list,
            is_archived=is_archived,
            is_deleted=False,
            is_shared=len(user_id_list) <= 1,
            user_id_list=user_id_list,
        )
        for (project_id, title, color, section_list, is_archived, user_id_list) in result
    ]), 200


#!/bin/bash

export PYTHONPATH=$PWD/../
export $(grep -v '^#' $PWD/../../.env.dev | xargs)

export POSTGRES_DB=${POSTGRES_DB_DEV}

export JWT_PUBLIC_KEY_PATH=$PWD/../jwt_keys/jwt_key.pub
export JWT_PRIVATE_KEY_PATH=$PWD/../jwt_keys/jwt_key
export FILE_STORE_PATH=$PWD/../../db_files/

python dev.py
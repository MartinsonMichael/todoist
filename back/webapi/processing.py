import os
import time
import hashlib
import zipfile

from typing import Dict, Tuple, Union
from sqlalchemy.engine import LegacyCursorResult
from sqlalchemy.sql import select, insert

from db.utils import Engine
from db.tables import UserDB, ProjectDB, TaskDB, ProjectPermission

import webapi.generated_messages as msg

env = os.environ


def get_password_hash(password: str) -> str:
    return hashlib.md5(
        password.encode('utf8')
        + "-networker".encode('utf8')
        + hashlib.md5("17".encode('utf8') + password.encode('utf8')).hexdigest().encode('utf8')
    ).hexdigest()


def registration(json_data: Dict) -> Tuple[Union[msg.AuthResponse, Dict], int]:
    """
    Process registration
    :param json_data: json of request body
    :return: Tuple[Dict to send, status code - int]
    """
    if 'username' not in json_data.keys() \
            or 'password' not in json_data.keys() \
            or 'email' not in json_data.keys():
        return {'msg': 'Expect body : {"email": str, "username": str, "password": str}'}, 400

    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        insert(UserDB).values({
            'email': json_data['email'],
            'password_hash': get_password_hash(json_data['password']),
            'username': json_data['username'],
        })
    )
    user_id = result.inserted_primary_key[0]

    _make_new_projects_for_user(user_id)

    return msg.AuthResponse(user_id=user_id, username=json_data['username'], access_token="", refresh_token=""), 200


def login(json_data: Dict) -> Tuple[Union[msg.AuthResponse, Dict], int]:
    """
    Process login
    :param json_data: json of request body
    :return: Tuple[Dict to send, status code - int]
    """
    if 'password' not in json_data.keys() or 'email' not in json_data.keys():
        return {'msg': 'Expect body : {"email": str, "password": str}'}, 400

    print(f"login -> got password hash: '{get_password_hash(json_data['password'])}'")

    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        select(UserDB.c.user_id, UserDB.c.username)
        .where(
            UserDB.c.password_hash == get_password_hash(json_data['password']),
            UserDB.c.email == json_data['email'],
        )
    )
    if result.rowcount == 0:
        return {'msg': "No such user OR wrong password"}, 401
    if result.rowcount > 1:
        raise ValueError("PANIC, more then 1 user with same emails")
    user_id, username = result.first()
    return msg.AuthResponse(user_id=user_id, username=username, access_token="", refresh_token=""), 200


def _make_new_projects_for_user(user_id: int) -> None:
    connection = Engine.connect()

    # creating inbox
    result: LegacyCursorResult = connection.execute(
        insert(ProjectDB).values({
            'user_id': user_id,
            'title': "Inbox",
            'color': 'blue',
        })
    )
    inbox_project_id = result.inserted_primary_key[0]
    result: LegacyCursorResult = connection.execute(
        insert(ProjectPermission).values({
            'user_id': user_id,
            'project_id': inbox_project_id,
        })
    )

    # inserting couple of starting tasks
    result: LegacyCursorResult = connection.execute(
        insert(TaskDB).values([
            {
                'project_id': inbox_project_id,
                'title': 'Learn how make new projects',
                'description': 'Simple press "+" button near "Project" column'
            },
            {
                'project_id': inbox_project_id,
                'title': 'Learn how to add new tasks',
                'description': "well, didn't implemented yet",
            },
            {
                'project_id': inbox_project_id,
                'title': 'Priority',
                'description': (
                    "Some tasks could be more or less important to you, "
                    "priority could be any integer in a range from 1 to 10"
                ),
                'priority': 2,
            },
        ])
    )

    # task with subtasks
    result: LegacyCursorResult = connection.execute(
        insert(TaskDB).values({
            'project_id': inbox_project_id,
            'title': 'You can add subtasks for any task',
            'description': 'Open with task for more info'
        })
    )
    parent_task_id = result.inserted_primary_key[0]
    result: LegacyCursorResult = connection.execute(
        insert(TaskDB).values([
            {
                'project_id': inbox_project_id,
                'parent_task_id': parent_task_id,
                'title': 'This is a subtask',
                'description': 'But it also a task! You could add subtasks here too!'
            },
            {
                'project_id': inbox_project_id,
                'parent_task_id': parent_task_id,
                'title': 'Another subtask',
                'description': "",
            },

        ])
    )

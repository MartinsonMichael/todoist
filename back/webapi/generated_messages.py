# This file is generated, DO NOT EDIT IT
# Michael Martinson http generator (c)
#

from typing import List, Dict, Union, Any, Optional, Dict
import json


class RegisterRequest:
    def __init__(self, email: str, username: str, password: str):
        self.email: str = email
        self.username: str = username
        self.password: str = password

    def to_json(self) -> Union[Dict, List]:
        return {
            'email': self.email,
            'username': self.username,
            'password': self.password,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'RegisterRequest':
        return RegisterRequest(
            email=obj['email'],
            username=obj['username'],
            password=obj['password'],
        )


class LoginRequest:
    def __init__(self, email: str, password: str):
        self.email: str = email
        self.password: str = password

    def to_json(self) -> Union[Dict, List]:
        return {
            'email': self.email,
            'password': self.password,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'LoginRequest':
        return LoginRequest(
            email=obj['email'],
            password=obj['password'],
        )


class AuthResponse:
    def __init__(self, user_id: int, username: str, access_token: str, refresh_token: str):
        self.user_id: int = user_id
        self.username: str = username
        self.access_token: str = access_token
        self.refresh_token: str = refresh_token

    def to_json(self) -> Union[Dict, List]:
        return {
            'user_id': self.user_id,
            'username': self.username,
            'access_token': self.access_token,
            'refresh_token': self.refresh_token,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'AuthResponse':
        return AuthResponse(
            user_id=obj['user_id'],
            username=obj['username'],
            access_token=obj['access_token'],
            refresh_token=obj['refresh_token'],
        )


class Token:
    def __init__(self, access_token: str):
        self.access_token: str = access_token

    def to_json(self) -> Union[Dict, List]:
        return {
            'access_token': self.access_token,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Token':
        return Token(
            access_token=obj['access_token'],
        )


class Task:
    def __init__(self, task_id: int, project_id: int, project_section: str, title: str, description: str, priority: int, utc_deadline_dttm: int, utc_assigned_dttm: int, subtask_id_list: List[int], is_deleted: bool, is_completed: bool, parent_task_id: Optional[int] = None):
        self.task_id: int = task_id
        self.project_id: int = project_id
        self.project_section: str = project_section
        self.title: str = title
        self.description: str = description
        self.priority: int = priority
        self.utc_deadline_dttm: int = utc_deadline_dttm
        self.utc_assigned_dttm: int = utc_assigned_dttm
        self.parent_task_id: Optional[int] = parent_task_id
        self.subtask_id_list: List[int] = subtask_id_list
        self.is_deleted: bool = is_deleted
        self.is_completed: bool = is_completed

    def to_json(self) -> Union[Dict, List]:
        return {
            'task_id': self.task_id,
            'project_id': self.project_id,
            'project_section': self.project_section,
            'title': self.title,
            'description': self.description,
            'priority': self.priority,
            'utc_deadline_dttm': self.utc_deadline_dttm,
            'utc_assigned_dttm': self.utc_assigned_dttm,
            'parent_task_id': self.parent_task_id if self.parent_task_id is not None else None,
            'subtask_id_list': self.subtask_id_list,
            'is_deleted': self.is_deleted,
            'is_completed': self.is_completed,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Task':
        return Task(
            task_id=obj['task_id'],
            project_id=obj['project_id'],
            project_section=obj['project_section'],
            title=obj['title'],
            description=obj['description'],
            priority=obj['priority'],
            utc_deadline_dttm=obj['utc_deadline_dttm'],
            utc_assigned_dttm=obj['utc_assigned_dttm'],
            parent_task_id=obj['parent_task_id'] if 'parent_task_id' in obj.keys() else None,
            subtask_id_list=obj['subtask_id_list'],
            is_deleted=obj['is_deleted'],
            is_completed=obj['is_completed'],
        )


class NewTask:
    def __init__(self, title: str, description: str, parent_task_id: Optional[int] = None, project_id: Optional[int] = None, project_section: Optional[str] = None, priority: Optional[int] = None, utc_deadline_dttm: Optional[int] = None, utc_assigned_dttm: Optional[int] = None):
        self.title: str = title
        self.description: str = description
        self.parent_task_id: Optional[int] = parent_task_id
        self.project_id: Optional[int] = project_id
        self.project_section: Optional[str] = project_section
        self.priority: Optional[int] = priority
        self.utc_deadline_dttm: Optional[int] = utc_deadline_dttm
        self.utc_assigned_dttm: Optional[int] = utc_assigned_dttm

    def to_json(self) -> Union[Dict, List]:
        return {
            'title': self.title,
            'description': self.description,
            'parent_task_id': self.parent_task_id if self.parent_task_id is not None else None,
            'project_id': self.project_id if self.project_id is not None else None,
            'project_section': self.project_section if self.project_section is not None else None,
            'priority': self.priority if self.priority is not None else None,
            'utc_deadline_dttm': self.utc_deadline_dttm if self.utc_deadline_dttm is not None else None,
            'utc_assigned_dttm': self.utc_assigned_dttm if self.utc_assigned_dttm is not None else None,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'NewTask':
        return NewTask(
            title=obj['title'],
            description=obj['description'],
            parent_task_id=obj['parent_task_id'] if 'parent_task_id' in obj.keys() else None,
            project_id=obj['project_id'] if 'project_id' in obj.keys() else None,
            project_section=obj['project_section'] if 'project_section' in obj.keys() else None,
            priority=obj['priority'] if 'priority' in obj.keys() else None,
            utc_deadline_dttm=obj['utc_deadline_dttm'] if 'utc_deadline_dttm' in obj.keys() else None,
            utc_assigned_dttm=obj['utc_assigned_dttm'] if 'utc_assigned_dttm' in obj.keys() else None,
        )


class TaskID:
    def __init__(self, task_id: int):
        self.task_id: int = task_id

    def to_json(self) -> Union[Dict, List]:
        return {
            'task_id': self.task_id,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'TaskID':
        return TaskID(
            task_id=obj['task_id'],
        )


class TaskList:
    def __init__(self, list: List['Task']):
        self.list: List['Task'] = list

    def to_json(self) -> Union[Dict, List]:
        return {
            'list': [x.to_json() for x in self.list],
        }

    @staticmethod
    def from_json(obj: Dict) -> 'TaskList':
        return TaskList(
            list=[Task.from_json(x) for x in obj['list']],
        )


class Project:
    def __init__(self, project_id: int, title: str, color: str, section_list: List[str], is_shared: bool, user_id_list: List[int], is_archived: bool, is_deleted: bool):
        self.project_id: int = project_id
        self.title: str = title
        self.color: str = color
        self.section_list: List[str] = section_list
        self.is_shared: bool = is_shared
        self.user_id_list: List[int] = user_id_list
        self.is_archived: bool = is_archived
        self.is_deleted: bool = is_deleted

    def to_json(self) -> Union[Dict, List]:
        return {
            'project_id': self.project_id,
            'title': self.title,
            'color': self.color,
            'section_list': self.section_list,
            'is_shared': self.is_shared,
            'user_id_list': self.user_id_list,
            'is_archived': self.is_archived,
            'is_deleted': self.is_deleted,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Project':
        return Project(
            project_id=obj['project_id'],
            title=obj['title'],
            color=obj['color'],
            section_list=obj['section_list'],
            is_shared=obj['is_shared'],
            user_id_list=obj['user_id_list'],
            is_archived=obj['is_archived'],
            is_deleted=obj['is_deleted'],
        )


class ProjectList:
    def __init__(self, list: List['Project']):
        self.list: List['Project'] = list

    def to_json(self) -> Union[Dict, List]:
        return {
            'list': [x.to_json() for x in self.list],
        }

    @staticmethod
    def from_json(obj: Dict) -> 'ProjectList':
        return ProjectList(
            list=[Project.from_json(x) for x in obj['list']],
        )


class NewProject:
    def __init__(self, title: str, color: str):
        self.title: str = title
        self.color: str = color

    def to_json(self) -> Union[Dict, List]:
        return {
            'title': self.title,
            'color': self.color,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'NewProject':
        return NewProject(
            title=obj['title'],
            color=obj['color'],
        )


class ProjectID:
    def __init__(self, project_id: int):
        self.project_id: int = project_id

    def to_json(self) -> Union[Dict, List]:
        return {
            'project_id': self.project_id,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'ProjectID':
        return ProjectID(
            project_id=obj['project_id'],
        )


class Activity:
    def __init__(self, user_id: int, task_id: int, change: str):
        self.user_id: int = user_id
        self.task_id: int = task_id
        self.change: str = change

    def to_json(self) -> Union[Dict, List]:
        return {
            'user_id': self.user_id,
            'task_id': self.task_id,
            'change': self.change,
        }

    @staticmethod
    def from_json(obj: Dict) -> 'Activity':
        return Activity(
            user_id=obj['user_id'],
            task_id=obj['task_id'],
            change=obj['change'],
        )


class ActivityList:
    def __init__(self, list: List['Activity']):
        self.list: List['Activity'] = list

    def to_json(self) -> Union[Dict, List]:
        return {
            'list': [x.to_json() for x in self.list],
        }

    @staticmethod
    def from_json(obj: Dict) -> 'ActivityList':
        return ActivityList(
            list=[Activity.from_json(x) for x in obj['list']],
        )



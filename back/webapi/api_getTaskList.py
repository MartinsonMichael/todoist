from sqlalchemy import select
from sqlalchemy.engine import LegacyCursorResult
from typing import Tuple, Union
import webapi.generated_messages as msg
from db.tables import ProjectDB, TaskDB, ProjectPermission
from db.utils import Engine
from webapi.get_task_as_msg import task_as_msg


def getTaskList(user_id: int) -> Tuple[Union[msg.TaskList, str], int]:
    """
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.TaskList | string - msg.TaskList if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        select(TaskDB.c.task_id)
        .where(
            TaskDB.c.project_id == ProjectDB.c.project_id,
            ProjectDB.c.project_id == ProjectPermission.c.project_id,
            ProjectPermission.c.user_id == user_id,
        )
    )

    return msg.TaskList(list=task_as_msg([x[0] for x in result])), 200


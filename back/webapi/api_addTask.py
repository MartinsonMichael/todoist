import datetime
from sqlalchemy import insert, select
from sqlalchemy.engine import LegacyCursorResult
from typing import Tuple, Union
import webapi.generated_messages as msg
from db._tables.todoist import TaskDB, ProjectDB
from db.utils import Engine
from webapi.get_task_as_msg import task_as_msg


def addTask(input_data: msg.NewTask, user_id: int) -> Tuple[Union[msg.Task, str], int]:
    """
    :param input_data: object of type NewTask (see generated methods for more info)
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.Task | string - msg.Task if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """
    connection = Engine.connect()
    project_id = input_data.project_id
    if project_id is None:
        result: LegacyCursorResult = connection.execute(
            select(ProjectDB.c.project_id).where(ProjectDB.c.user_id == user_id, ProjectDB.c.title == "Inbox")
        )
        assert result.rowcount == 1, f"should be 1, got {result.rowcount}"
        project_id = result.first()[0]

    result: LegacyCursorResult = connection.execute(
        insert(TaskDB).values({
            'project_id': project_id,
            'parent_task_id': input_data.parent_task_id,
            'project_section': input_data.project_section,
            'title': input_data.title,
            'description': input_data.description,
            'priority': input_data.priority if input_data.priority is not None else 5,
            'utc_deadline_dttm': (
                datetime.datetime.fromtimestamp(input_data.utc_deadline_dttm)
                if input_data.utc_deadline_dttm is not None
                else None
            ),
            'utc_assigned_dttm': (
                datetime.datetime.fromtimestamp(input_data.utc_deadline_dttm)
                if input_data.utc_assigned_dttm is not None
                else None
            ),
        })
    )
    task_id = result.inserted_primary_key[0]

    return task_as_msg([task_id])[0], 200


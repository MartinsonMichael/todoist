from sqlalchemy import update, select, delete, insert
from sqlalchemy.engine import LegacyCursorResult
from typing import Tuple, Union
import webapi.generated_messages as msg
from db.tables import ProjectDB, ProjectPermission
from db.utils import Engine


def updateProject(input_data: msg.Project, user_id: int) -> Tuple[Union[msg.Project, str], int]:
    """
    :param input_data: object of type ProjectUpdate (see generated methods for more info)
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.Project | string - msg.Project if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        update(ProjectDB)
        .where(
            ProjectPermission.c.user_id == user_id,
            ProjectDB.c.project_id == input_data.project_id,
            ProjectPermission.c.project_id == ProjectDB.c.project_id,
        )
        .values({
            'title': input_data.title,
            'color': input_data.color,
            'section_list': input_data.section_list,
            'is_archived': input_data.is_archived,
        })
    )
    if result.rowcount == 0:
        return {'msg': 'Permission denied'}, 401
    result: LegacyCursorResult = connection.execute(
        select(ProjectPermission.c.user_id).where(ProjectPermission.c.project_id == input_data.project_id)
    )
    old_user_id_list = [x[0] for x in result]

    # remove permission
    users_to_delete = []
    for old_user_id in old_user_id_list:
        if old_user_id not in input_data.user_id_list:
            users_to_delete.append(old_user_id)

    if len(users_to_delete) != 0:
        connection.execute(
            delete(ProjectPermission).where(
                ProjectPermission.c.project_id == input_data.project_id,
                ProjectPermission.c.user_id.in_(users_to_delete)
            )
        )

    # add permission
    users_to_add = []
    for new_user_id in input_data.user_id_list:
        if new_user_id not in old_user_id_list:
            users_to_add.append(new_user_id)

    if len(users_to_add) != 0:
        connection.execute(
            insert(ProjectPermission).values([
                {'project_id': input_data.project_id, 'user_id': new_user_id}
                for new_user_id in users_to_add
            ])
        )

    return input_data, 200

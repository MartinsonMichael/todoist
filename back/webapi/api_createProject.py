from sqlalchemy import insert
from sqlalchemy.engine import LegacyCursorResult
from typing import Tuple, Union
import webapi.generated_messages as msg
from db.tables import ProjectDB, ProjectPermission
from db.utils import Engine


def createProject(input_data: msg.NewProject, user_id: int) -> Tuple[Union[msg.Project, str], int]:
    """
    :param input_data: object of type NewProject (see generated methods for more info)
    :param user_id: int - id of user who perform this api method
    :return  Tuple of two objects:
         * first - msg.Project | string - msg.Project if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """
    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        insert(ProjectDB).values({
            'user_id': user_id,
            'title': input_data.title,
            'color': input_data.color,
        })
    )
    project_id = result.inserted_primary_key[0]

    connection.execute(
        insert(ProjectPermission).values({
            'project_id': project_id,
            'user_id': user_id,
        })
    )

    return msg.Project(
        project_id=project_id,
        title=input_data.title,
        color=input_data.color,
        section_list=[],
        is_shared=False,
        user_id_list=[user_id],
        is_archived=False,
        is_deleted=False,
    ), 200

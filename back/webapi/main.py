import logging
import datetime
import os

import flask
from flask_cors import CORS
from flask import request, jsonify, Response
from typing import Tuple, Union
from werkzeug.utils import secure_filename

from flask_jwt_extended import (
    create_access_token, get_jwt_identity, jwt_required, JWTManager,
    create_refresh_token, get_jwt, set_access_cookies, set_refresh_cookies,
)
import webapi.processing as utils

from redis_cache.utils import redis_client

FILE_STORE_PATH = os.environ['FILE_STORE_PATH']
ALLOWED_EXTENSIONS = {'zip'}

app = flask.Flask(__name__)
app.config['UPLOAD_FOLDER'] = FILE_STORE_PATH

app.config["JWT_SECRET_KEY"] = open(os.environ['JWT_PRIVATE_KEY_PATH'], 'rb').read()
app.config["JWT_ACCESS_COOKIE_NAME"] = "access_token"
app.config["JWT_ACCESS_TOKEN_EXPIRES"] = datetime.timedelta(hours=3)
app.config["JWT_REFRESH_COOKIE_NAME"] = "refresh_token"
app.config["JWT_REFRESH_TOKEN_EXPIRES"] = datetime.timedelta(days=60)
app.config["JWT_CSRF_IN_COOKIES"] = True
jwt = JWTManager(app)
CORS(app, supports_credentials=True, expose_headers=['access_header', 'refresh_header'])

logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(os.environ.get("LOGGING_LEVEL", "DEBUG"))


@app.after_request
def refresh_expiring_access(response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.datetime.now(datetime.timezone.utc)
        target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(minutes=30))
        if target_timestamp > exp_timestamp:
            user_id: int = int(get_jwt_identity())
            access_token = create_access_token(identity=user_id, fresh=True)
            response.headers['access_header'] = access_token
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original response
        return response


@app.after_request
def refresh_expiring_refresh(response):
    try:
        exp_timestamp = get_jwt()["exp"]
        now = datetime.datetime.now(datetime.timezone.utc)
        target_timestamp = datetime.datetime.timestamp(now + datetime.timedelta(days=10))
        if target_timestamp > exp_timestamp:
            user_id: int = int(get_jwt_identity())
            refresh_token = create_refresh_token(identity=user_id)
            response.headers['refresh_header'] = refresh_token
        return response
    except (RuntimeError, KeyError):
        # Case where there is not a valid JWT. Just return the original response
        return response


@app.post("/api/register")
def register():
    """
    Expect body : {"email": str, "username": str, "password": str}
    :return: {"access_token": str}

    NOTE: frontend must set header: {'Authorization' "Bearer <value of access_token/refresh_token>"}
        exactly with way, with a space after "Bearer"
    """
    data, code = utils.registration(request.json)
    if code != 200:
        return data, code

    data.access_token = create_access_token(identity=data.user_id, fresh=True)
    data.refresh_token = create_refresh_token(identity=data.user_id)
    return jsonify(data.to_json()), 200


@app.post("/api/login")
def login():
    """
    Expect body : {"email": str, "password": str}
    :return: {"access_token": str, "refresh_token": str}

    NOTE: frontend must set header: {'Authorization' "Bearer <value of access_token/refresh_token>"}
        exactly with way, with a space after "Bearer"
    """
    data, code = utils.login(request.json)
    if code != 200:
        return data, code

    data.access_token = create_access_token(identity=data.user_id, fresh=True)
    data.refresh_token = create_refresh_token(identity=data.user_id)
    return jsonify(data.to_json()), 200


@app.post("/api/refreshToken")
@jwt_required(refresh=True)
def updateToken():
    """
    Expect body : {}
    :return: {"access_token": str}

    NOTE: frontend must set header: {'Authorization' "Bearer <value of access_token/refresh_token>"}
        exactly with way, with a space after "Bearer"
    """

    user_id: int = int(get_jwt_identity())
    access_token = create_access_token(identity=user_id, fresh=True)
    return jsonify(msg.Token(access_token=access_token).to_json()), 200


# --- START OF GENERATED PART: DO NOT EDIT CODE BELLOW --- #


import webapi.generated_messages as msg
from webapi.api_getTaskList import getTaskList
from webapi.api_addTask import addTask
from webapi.api_updateTask import updateTask
from webapi.api_deleteTask import deleteTask
from webapi.api_completeTask import completeTask
from webapi.api_getProjectList import getProjectList
from webapi.api_createProject import createProject
from webapi.api_updateProject import updateProject
from webapi.api_archiveProject import archiveProject
from webapi.api_deleteProject import deleteProject
from webapi.api_getChangeLog import getChangeLog


@app.post('/api/getTaskList')
@jwt_required()
def api_method_getTaskList() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    output_data, code = getTaskList(user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.TaskList), \
        f"Wrong type of output_data, should be 'TaskList', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/addTask')
@jwt_required()
def api_method_addTask() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.NewTask = msg.NewTask.from_json(request.json)
    output_data, code = addTask(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.Task), \
        f"Wrong type of output_data, should be 'Task', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/updateTask')
@jwt_required()
def api_method_updateTask() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.Task = msg.Task.from_json(request.json)
    output_data, code = updateTask(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.Task), \
        f"Wrong type of output_data, should be 'Task', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/deleteTask')
@jwt_required()
def api_method_deleteTask() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.TaskID = msg.TaskID.from_json(request.json)
    output_data, code = deleteTask(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.TaskID), \
        f"Wrong type of output_data, should be 'TaskID', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/completeTask')
@jwt_required()
def api_method_completeTask() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.TaskID = msg.TaskID.from_json(request.json)
    output_data, code = completeTask(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.TaskID), \
        f"Wrong type of output_data, should be 'TaskID', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/getProjectList')
@jwt_required()
def api_method_getProjectList() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    output_data, code = getProjectList(user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.ProjectList), \
        f"Wrong type of output_data, should be 'ProjectList', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/createProject')
@jwt_required()
def api_method_createProject() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.NewProject = msg.NewProject.from_json(request.json)
    output_data, code = createProject(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.Project), \
        f"Wrong type of output_data, should be 'Project', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/updateProject')
@jwt_required()
def api_method_updateProject() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.Project = msg.Project.from_json(request.json)
    output_data, code = updateProject(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.Project), \
        f"Wrong type of output_data, should be 'Project', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/archiveProject')
@jwt_required()
def api_method_archiveProject() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.ProjectID = msg.ProjectID.from_json(request.json)
    output_data, code = archiveProject(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.ProjectID), \
        f"Wrong type of output_data, should be 'ProjectID', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/deleteProject')
@jwt_required()
def api_method_deleteProject() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    input_data: msg.ProjectID = msg.ProjectID.from_json(request.json)
    output_data, code = deleteProject(input_data, user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.ProjectID), \
        f"Wrong type of output_data, should be 'ProjectID', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


@app.post('/api/getChangeLog')
@jwt_required()
def api_method_getChangeLog() -> Tuple[Union[Response, str], int]:
    """
    TODO @MichaelMD add comment auto generation
    """
    user_id: int = int(get_jwt_identity())
    output_data, code = getChangeLog(user_id)
    if code < 200 or 300 <= code:
        return output_data, code
    assert isinstance(output_data, msg.ActivityList), \
        f"Wrong type of output_data, should be 'ActivityList', got {type(output_data)}"
    return jsonify(output_data.to_json()), code


# --- START OF GENERATED PART: DO NOT EDIT CODE UPPER --- #



@app.get("/health")
@app.get("/api/health")
def get_health_status():
    return "Tamplete:WebAPI - OK", 200


@app.errorhandler(404)
def error(err):
    return "wrong url. Try `/health`", 404

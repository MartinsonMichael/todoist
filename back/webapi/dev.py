import os
from webapi.main import app

if __name__ == "__main__":

    app.run(
        'localhost',
        port=os.environ['WEBAPI_INNER_PORT'],  # 8102
        debug=True,
    )

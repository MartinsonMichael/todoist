from typing import Optional, List

from sqlalchemy import select
from sqlalchemy.dialects.postgresql import array_agg
from sqlalchemy.engine import LegacyCursorResult

from db.utils import Engine
from db.tables import TaskDB

import webapi.generated_messages as msg


def task_as_msg(task_id_list: List[int]) -> Optional[List[msg.Task]]:
    connection = Engine.connect()
    result: LegacyCursorResult = connection.execute(
        select(
            TaskDB.c.task_id, TaskDB.c.parent_task_id,
            TaskDB.c.project_id, TaskDB.c.project_section,
            TaskDB.c.title, TaskDB.c.description,
            TaskDB.c.priority, TaskDB.c.utc_deadline_dttm, TaskDB.c.utc_assigned_dttm,
            TaskDB.c.is_deleted, TaskDB.c.is_completed,
            # array_agg()
        )
        .where(TaskDB.c.task_id.in_(task_id_list))
    )
    if result.rowcount == 0:
        return None

    task_msg_list = []
    for (task_id, parent_task_id, project_id, project_section, title, description, priority,
        utc_deadline_dttm, utc_assigned_dttm, is_deleted, is_completed) in result:

        result: LegacyCursorResult = connection.execute(
            select(
                TaskDB.c.task_id
            )
            .where(TaskDB.c.parent_task_id == task_id)
        )
        subtask_id_list = [x[0] for x in result]

        task_msg_list.append(
            msg.Task(
                task_id=task_id,
                parent_task_id=parent_task_id,
                project_id=project_id,
                project_section=project_section,
                title=title,
                description=description,
                priority=priority,
                utc_deadline_dttm=utc_deadline_dttm,
                utc_assigned_dttm=utc_assigned_dttm,
                subtask_id_list=subtask_id_list,
                is_deleted=is_deleted,
                is_completed=is_completed,
            )
        )
    return task_msg_list

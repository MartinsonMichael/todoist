from typing import Tuple, Union
import webapi.generated_messages as msg
from werkzeug.datastructures import ImmutableMultiDict, FileStorage


def makeNewTaskFromFile(user_id: int, file_dict: ImmutableMultiDict[str, FileStorage]) -> Tuple[Union[msg.Task, str], int]:
    """
    :param user_id: int - id of user who perform this api method
    :param file_dict: Dict[str, FileStorage] - input files, map of name to file object
    :return  Tuple of two objects:
         * first - msg.Task | string - msg.Task if function call was successful,
            string with error description otherwise
         * second - int - http code, if code not in range 200-299,
            then the first object expected to be a string with error description
    """

    raise NotImplemented


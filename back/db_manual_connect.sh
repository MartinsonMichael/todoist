#!/bin/bash

export $(grep -v '^#' $PWD/../.env.dev | xargs)

export POSTGRES_DB=${POSTGRES_DB_DEV}
export PGPASSWORD=${POSTGRES_PASSWORD}

echo "Using params: -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -d ${POSTGRES_DB}"

psql -U ${POSTGRES_USER} -h ${POSTGRES_HOST} -p ${POSTGRES_PORT} -d ${POSTGRES_DB}

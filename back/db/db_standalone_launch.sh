#!/bin/bash

echo "Exporting DEV env variables..."
export $(grep -v '^#' ../../.env.dev | xargs)
export POSTGRES_DB=${POSTGRES_DB_DEV}

docker run -it \
  --rm --name db \
  -e POSTGRES_USER=${POSTGRES_USER} \
  -e POSTGRES_PASSWORD=${POSTGRES_PASSWORD} \
  -e POSTGRES_DB=${POSTGRES_DB} \
  -p ${POSTGRES_PORT}:5432 \
  -v template_dev_db:/var/lib/postgresql/data/ \
  postgres:12.0-alpine
from typing import Optional, Dict, Union
from ._tables.user import UserDB
from ._tables.todoist import ProjectDB, TaskDB, ProjectPermission


from db.utils import Engine, _meta_data
_meta_data.create_all(Engine)

import os
import sqlalchemy
import datetime
from typing import Optional

env = os.environ

DB_URL = (
    f"postgresql+psycopg2://{env['POSTGRES_USER']}:{env['POSTGRES_PASSWORD']}@{env['POSTGRES_HOST']}:{env['POSTGRES_PORT']}/{env['POSTGRES_DB']}"
)


Engine = sqlalchemy.create_engine(DB_URL)
_meta_data = sqlalchemy.MetaData(Engine)

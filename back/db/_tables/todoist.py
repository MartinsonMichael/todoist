import sqlalchemy as sql
import sqlalchemy_utils as sql_utils
from sqlalchemy import Table, Column

from sqlalchemy.dialects.postgresql import ARRAY, JSON
from sqlalchemy.sql.functions import now

from db.utils import _meta_data


ProjectDB = Table(
    'project', _meta_data,
    Column('project_id', sql.Integer, primary_key=True, autoincrement=True),
    Column('user_id', sql.Integer, sql.ForeignKey('userdb.user_id', ondelete='CASCADE')),

    Column('title', sql.String, nullable=False),
    Column('color', sql.String(length=32), default="red"),
    Column('section_list', ARRAY(sql.String(length=64)), default=[]),

    Column('is_archived', sql.Boolean, default=False),
)

ProjectPermission = Table(
    'project_permission', _meta_data,
    Column('permission_id', sql.Integer, primary_key=True, autoincrement=True),
    Column('project_id', sql.Integer, sql.ForeignKey('project.project_id', ondelete='CASCADE'), nullable=False),
    Column('user_id', sql.Integer, sql.ForeignKey('userdb.user_id', ondelete='CASCADE'), nullable=False),
)


TaskDB = Table(
    'task', _meta_data,
    Column('task_id', sql.Integer, primary_key=True, autoincrement=True),
    Column('parent_task_id', sql.Integer, sql.ForeignKey('task.task_id', ondelete='CASCADE'), nullable=True),

    Column('project_id', sql.Integer, sql.ForeignKey('project.project_id', ondelete='CASCADE'), nullable=True),
    Column('project_section', sql.String(64), nullable=True),

    Column('title', sql.String, nullable=False),
    Column('description', sql.String, default="", nullable=False),

    Column('priority', sql.Integer, default=5, nullable=False),

    Column('utc_deadline_dttm', sql.DateTime, nullable=True),
    Column('utc_assigned_dttm', sql.DateTime, nullable=True),

    Column('is_completed', sql.Boolean, default=False),
    Column('is_deleted', sql.Boolean, default=False),
    Column('is_archived', sql.Boolean, default=False),
)
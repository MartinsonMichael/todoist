import sqlalchemy as sql
import sqlalchemy_utils as sql_utils
from sqlalchemy import Table, Column

from sqlalchemy.dialects.postgresql import ARRAY, JSON
from sqlalchemy.sql.functions import now

from db.utils import _meta_data


UserDB = Table(
    'userdb', _meta_data,
    Column('user_id', sql.Integer, primary_key=True, autoincrement=True),
    Column('email', sql_utils.EmailType, nullable=False, unique=True),
    Column('username', sql.String(length=64), nullable=False),
    Column('password_hash', sql.String, nullable=False),
)

#!/bin/bash

export $(grep -v '^#' $PWD/../.env.dev | xargs)
export POSTGRES_DB=${POSTGRES_DB_DEV}

export FILE_STORE_PATH=$PWD/../db_files/
export JWT_PUBLIC_KEY_PATH=$PWD/keys/jwt_rsa.pub
export JWT_PRIVATE_KEY_PATH=$PWD/keys/jwt_rsa

python

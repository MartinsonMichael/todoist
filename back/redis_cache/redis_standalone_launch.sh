#!/bin/bash

export $(grep -v '^#' ../../.env.dev | xargs)

docker run -it \
  --rm --name redis \
  -p ${REDIS_PORT}:6379 \
  -v ${PWD}/redis.conf:/usr/local/etc/redis/redis.conf \
  redis:6.2.6-alpine redis-server /usr/local/etc/redis/redis.conf

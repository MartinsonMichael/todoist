import os
import redis

env = os.environ

redis_client = redis.Redis(
    host=env['REDIS_HOST'],
    port=env['REDIS_PORT'],
    username=env['REDIS_USER'],
    password=env['REDIS_PASSWORD'],
)

#!/bin/bash

export CI_REGISTRY_IMAGE=registry.gitlab.com/martinsonmichael/<TODO>
echo "Using CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE}"

echo "Freezeing pip requirements into requirements.txt..."
pip freeze > requirements.txt

echo "Rebuilding image..."
docker build -f Dockerfile_CI_python -t $CI_REGISTRY_IMAGE/python-test .

echo "Pushing image into the gitlab registry..."
docker push $CI_REGISTRY_IMAGE/python-test
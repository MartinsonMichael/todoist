import React, {useEffect} from "react";
import {Route, Routes, useParams} from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import { Stack} from "react-bootstrap";
import SideBar from "../componets/SideBar";
import {getProjectList} from "../store/projectService/projectService_actions";
import {getTaskList} from "../store/taskService/taskService_actions";
import Register from "../componets/Register";
import TodayTaskViewer from "../componets/TodayTaskViewer";
import InboxTaskViewer from "../componets/InboxTaskViewer";
import ProjectTaskViewer from "../componets/ProjectTaskViewer";
import UpcomingTaskViewer from "../componets/UpcomingTaskViewer";


const mapStoreStateToProps = (store: RootState) => ({
    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
        getProjectList: () => dispatch(getProjectList()),
        getTaskList: () => dispatch(getTaskList()),
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type HomePageProps = ConnectedProps<typeof connector> & {};


const Home: React.FC<HomePageProps> = (props: HomePageProps): React.ReactElement => {

    useEffect(() => {
        props.getProjectList();
        props.getTaskList();
    });

    return (
        <Stack direction="horizontal">
            <SideBar/>
            <Stack
                className="ms-auto me-auto ps-4 pe-4 justify-content-start mw-74"
                direction="vertical"
            >
                <Routes>
                    <Route path="/inbox" element={<InboxTaskViewer/>}/>
                    <Route path="/today" element={<TodayTaskViewer/>}/>
                    <Route path="/upcoming" element={<UpcomingTaskViewer/>}/>
                    <Route path="/:project_id" element={<ProjectTaskViewer/>}/>
                </Routes>
            </Stack>
            <Register/>
        </Stack>
    );
};

export default connector(Home);

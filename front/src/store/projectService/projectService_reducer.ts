import * as msg from "../generated_messages"
import { ProjectServiceActionType } from "./projectService_actions"


export interface ProjectServiceState {
    projectList?: msg.Project[],
    inbox_project_id?: number,

    isLoading: boolean
    error?: string
}

const initialState: ProjectServiceState = {
    projectList: undefined,

    isLoading: false,
    error: undefined,
} as ProjectServiceState;


export function ProjectServiceReducer(state = initialState, action: ProjectServiceActionType): ProjectServiceState {
    switch (action.type) {
        case "getProjectList_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ProjectServiceState;

        case "getProjectList_SUCCESS":
            return {
                ...state,
                projectList: action.payload.list,
                inbox_project_id: (
                    action.payload.list
                        .filter((project: msg.Project) => project.title === "Inbox")[0].project_id
                ),
                isLoading: false,
                error: undefined,
            } as ProjectServiceState;

        case "getProjectList_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ProjectServiceState;


        case "createProject_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ProjectServiceState;

        case "createProject_SUCCESS":
            return {
                ...state,
                projectList: [
                    ...state.projectList || [],
                    action.payload,
                ],
                isLoading: false,
                error: undefined,
            } as ProjectServiceState;

        case "createProject_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ProjectServiceState;


        case "updateProject_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ProjectServiceState;

        case "updateProject_SUCCESS":
            const newProjectList: msg.Project[] = [...state.projectList || []];
            for (let i = 0; i < newProjectList.length; i++) {
                if (newProjectList[i].project_id === action.payload.project_id) {
                    newProjectList[i] = action.payload;
                    break;
                }
            }
            return {
                ...state,
                projectList: newProjectList,
                isLoading: false,
                error: undefined,
            } as ProjectServiceState;

        case "updateProject_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ProjectServiceState;


        case "archiveProject_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ProjectServiceState;

        case "archiveProject_SUCCESS":
            const newProjectList_archive: msg.Project[] = [...state.projectList || []];
            for (let i = 0; i < newProjectList_archive.length; i++) {
                if (newProjectList_archive[i].project_id === action.payload.project_id) {
                    newProjectList_archive[i].is_archived = true;
                    break;
                }
            }
            return {
                ...state,
                projectList: newProjectList_archive,
                isLoading: false,
                error: undefined,
            } as ProjectServiceState;

        case "archiveProject_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ProjectServiceState;


        case "deleteProject_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ProjectServiceState;

        case "deleteProject_SUCCESS":
            const newProjectList_deleted: msg.Project[] = [...state.projectList || []];
            for (let i = 0; i < newProjectList_deleted.length; i++) {
                if (newProjectList_deleted[i].project_id === action.payload.project_id) {
                    newProjectList_deleted[i].is_deleted = true;
                    break;
                }
            }
            return {
                ...state,
                projectList: newProjectList_deleted,
                isLoading: false,
                error: undefined,
            } as ProjectServiceState;

        case "deleteProject_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ProjectServiceState;


        default:
            return state
    }
}

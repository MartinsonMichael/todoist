// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const getProjectList_START = "getProjectList_START";
interface getProjectList_START_Action {
    type: typeof getProjectList_START
    payload: undefined
}
export const getProjectList_SUCCESS = "getProjectList_SUCCESS";
interface getProjectList_SUCCESS_Action {
    type: typeof getProjectList_SUCCESS
    payload: msg.ProjectList
}
export const getProjectList_REJECTED = "getProjectList_REJECTED";
interface getProjectList_REJECTED_Action {
    type: typeof getProjectList_REJECTED
    payload: string
}

export const getProjectList = () => {
    return async (dispatch: any) => {
        dispatch({type: getProjectList_START, payload: undefined});

        await axiosInstance.post(
            'getProjectList',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.ProjectList, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: getProjectList_SUCCESS, payload: msg.construct_ProjectList(response.data)});
                } else {
                    dispatch({type: getProjectList_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: getProjectList_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const createProject_START = "createProject_START";
interface createProject_START_Action {
    type: typeof createProject_START
    payload: undefined
}
export const createProject_SUCCESS = "createProject_SUCCESS";
interface createProject_SUCCESS_Action {
    type: typeof createProject_SUCCESS
    payload: msg.Project
}
export const createProject_REJECTED = "createProject_REJECTED";
interface createProject_REJECTED_Action {
    type: typeof createProject_REJECTED
    payload: string
}

export const createProject = (title: string, color: string) => {
    return async (dispatch: any) => {
        dispatch({type: createProject_START, payload: undefined});

        await axiosInstance.post(
            'createProject',
            {
                'title': title,
                'color': color,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Project, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: createProject_SUCCESS, payload: msg.construct_Project(response.data)});
                } else {
                    dispatch({type: createProject_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: createProject_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const updateProject_START = "updateProject_START";
interface updateProject_START_Action {
    type: typeof updateProject_START
    payload: undefined
}
export const updateProject_SUCCESS = "updateProject_SUCCESS";
interface updateProject_SUCCESS_Action {
    type: typeof updateProject_SUCCESS
    payload: msg.Project
}
export const updateProject_REJECTED = "updateProject_REJECTED";
interface updateProject_REJECTED_Action {
    type: typeof updateProject_REJECTED
    payload: string
}

export const updateProject = (project_id: number, title: string, color: string, section_list: string[], is_shared: boolean, user_id_list: number[], is_archived: boolean, is_deleted: boolean) => {
    return async (dispatch: any) => {
        dispatch({type: updateProject_START, payload: undefined});

        await axiosInstance.post(
            'updateProject',
            {
                'project_id': project_id,
                'title': title,
                'color': color,
                'section_list': section_list,
                'is_shared': is_shared,
                'user_id_list': user_id_list,
                'is_archived': is_archived,
                'is_deleted': is_deleted,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Project, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: updateProject_SUCCESS, payload: msg.construct_Project(response.data)});
                } else {
                    dispatch({type: updateProject_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: updateProject_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const archiveProject_START = "archiveProject_START";
interface archiveProject_START_Action {
    type: typeof archiveProject_START
    payload: undefined
}
export const archiveProject_SUCCESS = "archiveProject_SUCCESS";
interface archiveProject_SUCCESS_Action {
    type: typeof archiveProject_SUCCESS
    payload: msg.ProjectID
}
export const archiveProject_REJECTED = "archiveProject_REJECTED";
interface archiveProject_REJECTED_Action {
    type: typeof archiveProject_REJECTED
    payload: string
}

export const archiveProject = (project_id: number) => {
    return async (dispatch: any) => {
        dispatch({type: archiveProject_START, payload: undefined});

        await axiosInstance.post(
            'archiveProject',
            {
                'project_id': project_id,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.ProjectID, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: archiveProject_SUCCESS, payload: msg.construct_ProjectID(response.data)});
                } else {
                    dispatch({type: archiveProject_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: archiveProject_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const deleteProject_START = "deleteProject_START";
interface deleteProject_START_Action {
    type: typeof deleteProject_START
    payload: undefined
}
export const deleteProject_SUCCESS = "deleteProject_SUCCESS";
interface deleteProject_SUCCESS_Action {
    type: typeof deleteProject_SUCCESS
    payload: msg.ProjectID
}
export const deleteProject_REJECTED = "deleteProject_REJECTED";
interface deleteProject_REJECTED_Action {
    type: typeof deleteProject_REJECTED
    payload: string
}

export const deleteProject = (project_id: number) => {
    return async (dispatch: any) => {
        dispatch({type: deleteProject_START, payload: undefined});

        await axiosInstance.post(
            'deleteProject',
            {
                'project_id': project_id,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.ProjectID, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: deleteProject_SUCCESS, payload: msg.construct_ProjectID(response.data)});
                } else {
                    dispatch({type: deleteProject_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: deleteProject_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type ProjectServiceActionType = (
    getProjectList_START_Action |
    getProjectList_SUCCESS_Action |
    getProjectList_REJECTED_Action |
    createProject_START_Action |
    createProject_SUCCESS_Action |
    createProject_REJECTED_Action |
    updateProject_START_Action |
    updateProject_SUCCESS_Action |
    updateProject_REJECTED_Action |
    archiveProject_START_Action |
    archiveProject_SUCCESS_Action |
    archiveProject_REJECTED_Action |
    deleteProject_START_Action |
    deleteProject_SUCCESS_Action |
    deleteProject_REJECTED_Action 
)
import {combineReducers} from "redux";
import {AuthServiceReducer} from "./authService/authService_reducer";
import {ProjectServiceReducer} from "./projectService/projectService_reducer";
import {TaskServiceReducer} from "./taskService/taskService_reducer";


export const rootReducer = combineReducers({
    project: ProjectServiceReducer,
    task: TaskServiceReducer,
    auth: AuthServiceReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
import axios from "axios";
import { AxiosResponse, AxiosRequestConfig } from "axios";


export const axiosInstance = axios.create({
    baseURL: process.env.REACT_APP_API_BACKEND_ADDRESS + "/api/",
    responseType: "json",
    headers: {
        Authorization: "Bearer " + localStorage.getItem("access_token") || "",
    },
    withCredentials: true,
});


axiosInstance.interceptors.response.use(
    (response: AxiosResponse) => {

        // updating access token "on a fly", expect server to set this cookie if current token will expire soon
        if (response.headers && response.headers["access_header"] !== undefined) {
            const access_token = response.headers["access_header"];
            localStorage.setItem("access_token", access_token);
            axiosInstance.defaults.headers.common["Authorization"] = "Bearer " + access_token;
        }

        // updating refresh token "on a fly", expect server to set this cookie if current token will expire soon
        if (response.headers && response.headers["refresh_header"] !== undefined) {
            localStorage.setItem("refresh_token", response.headers["refresh_header"])
        }
        return response
    }
);

axiosInstance.interceptors.request.use(
    (request: AxiosRequestConfig) => {

        // using refresh_token instead on default access_token on a /api/refreshToken call
        if (request.url && request.url.includes("refreshToken")) {
            if (request.headers) {
                request.headers['Authorization'] = "Bearer " + localStorage.getItem("refresh_token")
            }
        }
        return request
    }
);

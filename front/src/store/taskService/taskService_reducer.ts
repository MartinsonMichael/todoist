import * as msg from "../generated_messages"
import { TaskServiceActionType } from "./taskService_actions"


export interface TaskServiceState {
    taskList?: msg.Task[],

    isLoading: boolean
    error?: string
}

const initialState: TaskServiceState = {
    // TODO add valuable state, probably rename

    isLoading: false,
    error: undefined,
} as TaskServiceState;


export function TaskServiceReducer(state = initialState, action: TaskServiceActionType): TaskServiceState {
    switch (action.type) {
        case "getTaskList_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "getTaskList_SUCCESS":
            return {
                ...state,
                taskList: action.payload.list,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "getTaskList_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "addTask_START":
            return {
                ...state,
                taskList: [
                    ...state.taskList || [],
                    action.payload,
                ],
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "addTask_SUCCESS":
            return {
                ...state,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "addTask_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "updateTask_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "updateTask_SUCCESS":
            const taskList_update: msg.Task[] = [...state.taskList || []];
            for (let i = 0; i < taskList_update.length; i++) {
                if (taskList_update[i].task_id === action.payload.task_id) {
                    taskList_update[i] = action.payload;
                }
            }
            return {
                ...state,
                taskList: taskList_update,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "updateTask_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "deleteTask_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "deleteTask_SUCCESS":
            const taskList_delete: msg.Task[] = [...state.taskList || []];
            for (let i = 0; i < taskList_delete.length; i++) {
                if (taskList_delete[i].task_id === action.payload.task_id) {
                    taskList_delete[i].is_deleted = true;
                }
            }
            return {
                ...state,
                taskList: taskList_delete,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "deleteTask_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        case "completeTask_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as TaskServiceState;

        case "completeTask_SUCCESS":
            const taskList_complete: msg.Task[] = (
                [...state.taskList || []]
            ).filter((task: msg.Task) => task.task_id !== action.payload.task_id);
            return {
                ...state,
                taskList: taskList_complete,
                isLoading: false,
                error: undefined,
            } as TaskServiceState;

        case "completeTask_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as TaskServiceState;


        default:
            return state
    }
}

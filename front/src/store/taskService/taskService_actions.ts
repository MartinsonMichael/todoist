// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const getTaskList_START = "getTaskList_START";
interface getTaskList_START_Action {
    type: typeof getTaskList_START
    payload: undefined
}
export const getTaskList_SUCCESS = "getTaskList_SUCCESS";
interface getTaskList_SUCCESS_Action {
    type: typeof getTaskList_SUCCESS
    payload: msg.TaskList
}
export const getTaskList_REJECTED = "getTaskList_REJECTED";
interface getTaskList_REJECTED_Action {
    type: typeof getTaskList_REJECTED
    payload: string
}

export const getTaskList = () => {
    return async (dispatch: any) => {
        dispatch({type: getTaskList_START, payload: undefined});

        await axiosInstance.post(
            'getTaskList',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.TaskList, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: getTaskList_SUCCESS, payload: msg.construct_TaskList(response.data)});
                } else {
                    dispatch({type: getTaskList_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: getTaskList_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const addTask_START = "addTask_START";
interface addTask_START_Action {
    type: typeof addTask_START
    payload: undefined
}
export const addTask_SUCCESS = "addTask_SUCCESS";
interface addTask_SUCCESS_Action {
    type: typeof addTask_SUCCESS
    payload: msg.Task
}
export const addTask_REJECTED = "addTask_REJECTED";
interface addTask_REJECTED_Action {
    type: typeof addTask_REJECTED
    payload: string
}

export const addTask = (title: string, description: string, parent_task_id?: number, project_id?: number, project_section?: string, priority?: number, utc_deadline_dttm?: number, utc_assigned_dttm?: number) => {
    return async (dispatch: any) => {
        dispatch({type: addTask_START, payload: undefined});

        await axiosInstance.post(
            'addTask',
            {
                'title': title,
                'description': description,
                'parent_task_id': parent_task_id,
                'project_id': project_id,
                'project_section': project_section,
                'priority': priority,
                'utc_deadline_dttm': utc_deadline_dttm,
                'utc_assigned_dttm': utc_assigned_dttm,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Task, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: addTask_SUCCESS, payload: msg.construct_Task(response.data)});
                } else {
                    dispatch({type: addTask_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: addTask_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const updateTask_START = "updateTask_START";
interface updateTask_START_Action {
    type: typeof updateTask_START
    payload: undefined
}
export const updateTask_SUCCESS = "updateTask_SUCCESS";
interface updateTask_SUCCESS_Action {
    type: typeof updateTask_SUCCESS
    payload: msg.Task
}
export const updateTask_REJECTED = "updateTask_REJECTED";
interface updateTask_REJECTED_Action {
    type: typeof updateTask_REJECTED
    payload: string
}

export const updateTask = (task_id: number, project_id: number, project_section: string, title: string, description: string, priority: number, utc_deadline_dttm: number, utc_assigned_dttm: number, subtask_id_list: number[], is_deleted: boolean, is_completed: boolean, parent_task_id?: number) => {
    return async (dispatch: any) => {
        dispatch({type: updateTask_START, payload: undefined});

        await axiosInstance.post(
            'updateTask',
            {
                'task_id': task_id,
                'project_id': project_id,
                'project_section': project_section,
                'title': title,
                'description': description,
                'priority': priority,
                'utc_deadline_dttm': utc_deadline_dttm,
                'utc_assigned_dttm': utc_assigned_dttm,
                'parent_task_id': parent_task_id,
                'subtask_id_list': subtask_id_list,
                'is_deleted': is_deleted,
                'is_completed': is_completed,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Task, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: updateTask_SUCCESS, payload: msg.construct_Task(response.data)});
                } else {
                    dispatch({type: updateTask_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: updateTask_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const deleteTask_START = "deleteTask_START";
interface deleteTask_START_Action {
    type: typeof deleteTask_START
    payload: undefined
}
export const deleteTask_SUCCESS = "deleteTask_SUCCESS";
interface deleteTask_SUCCESS_Action {
    type: typeof deleteTask_SUCCESS
    payload: msg.TaskID
}
export const deleteTask_REJECTED = "deleteTask_REJECTED";
interface deleteTask_REJECTED_Action {
    type: typeof deleteTask_REJECTED
    payload: string
}

export const deleteTask = (task_id: number) => {
    return async (dispatch: any) => {
        dispatch({type: deleteTask_START, payload: undefined});

        await axiosInstance.post(
            'deleteTask',
            {
                'task_id': task_id,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.TaskID, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: deleteTask_SUCCESS, payload: msg.construct_TaskID(response.data)});
                } else {
                    dispatch({type: deleteTask_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: deleteTask_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const completeTask_START = "completeTask_START";
interface completeTask_START_Action {
    type: typeof completeTask_START
    payload: undefined
}
export const completeTask_SUCCESS = "completeTask_SUCCESS";
interface completeTask_SUCCESS_Action {
    type: typeof completeTask_SUCCESS
    payload: msg.TaskID
}
export const completeTask_REJECTED = "completeTask_REJECTED";
interface completeTask_REJECTED_Action {
    type: typeof completeTask_REJECTED
    payload: string
}

export const completeTask = (task_id: number) => {
    return async (dispatch: any) => {
        dispatch({type: completeTask_START, payload: undefined});

        await axiosInstance.post(
            'completeTask',
            {
                'task_id': task_id,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.TaskID, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: completeTask_SUCCESS, payload: msg.construct_TaskID(response.data)});
                } else {
                    dispatch({type: completeTask_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: completeTask_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type TaskServiceActionType = (
    getTaskList_START_Action |
    getTaskList_SUCCESS_Action |
    getTaskList_REJECTED_Action |
    addTask_START_Action |
    addTask_SUCCESS_Action |
    addTask_REJECTED_Action |
    updateTask_START_Action |
    updateTask_SUCCESS_Action |
    updateTask_REJECTED_Action |
    deleteTask_START_Action |
    deleteTask_SUCCESS_Action |
    deleteTask_REJECTED_Action |
    completeTask_START_Action |
    completeTask_SUCCESS_Action |
    completeTask_REJECTED_Action 
)
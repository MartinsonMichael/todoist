// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const register_START = "register_START";
interface register_START_Action {
    type: typeof register_START
    payload: undefined
}
export const register_SUCCESS = "register_SUCCESS";
interface register_SUCCESS_Action {
    type: typeof register_SUCCESS
    payload: msg.AuthResponse
}
export const register_REJECTED = "register_REJECTED";
interface register_REJECTED_Action {
    type: typeof register_REJECTED
    payload: string
}

export const register = (email: string, username: string, password: string) => {
    return async (dispatch: any) => {
        dispatch({type: register_START, payload: undefined});

        await axiosInstance.post(
            'register',
            {
                'email': email,
                'username': username,
                'password': password,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.AuthResponse, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: register_SUCCESS, payload: msg.construct_AuthResponse(response.data)});
                } else {
                    dispatch({type: register_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: register_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const login_START = "login_START";
interface login_START_Action {
    type: typeof login_START
    payload: undefined
}
export const login_SUCCESS = "login_SUCCESS";
interface login_SUCCESS_Action {
    type: typeof login_SUCCESS
    payload: msg.AuthResponse
}
export const login_REJECTED = "login_REJECTED";
interface login_REJECTED_Action {
    type: typeof login_REJECTED
    payload: string
}

export const login = (email: string, password: string) => {
    return async (dispatch: any) => {
        dispatch({type: login_START, payload: undefined});

        await axiosInstance.post(
            'login',
            {
                'email': email,
                'password': password,
            },
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.AuthResponse, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: login_SUCCESS, payload: msg.construct_AuthResponse(response.data)});
                } else {
                    dispatch({type: login_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: login_REJECTED, payload: error.toString()});
            }
        );
    }
};


export const refreshToken_START = "refreshToken_START";
interface refreshToken_START_Action {
    type: typeof refreshToken_START
    payload: undefined
}
export const refreshToken_SUCCESS = "refreshToken_SUCCESS";
interface refreshToken_SUCCESS_Action {
    type: typeof refreshToken_SUCCESS
    payload: msg.Token
}
export const refreshToken_REJECTED = "refreshToken_REJECTED";
interface refreshToken_REJECTED_Action {
    type: typeof refreshToken_REJECTED
    payload: string
}

export const refreshToken = () => {
    return async (dispatch: any) => {
        dispatch({type: refreshToken_START, payload: undefined});

        await axiosInstance.post(
            'refreshToken',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.Token, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: refreshToken_SUCCESS, payload: msg.construct_Token(response.data)});
                } else {
                    dispatch({type: refreshToken_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: refreshToken_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type AuthServiceActionType = (
    register_START_Action |
    register_SUCCESS_Action |
    register_REJECTED_Action |
    login_START_Action |
    login_SUCCESS_Action |
    login_REJECTED_Action |
    refreshToken_START_Action |
    refreshToken_SUCCESS_Action |
    refreshToken_REJECTED_Action 
)
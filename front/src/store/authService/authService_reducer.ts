import * as msg from "../generated_messages";
import { AuthServiceActionType } from "./authService_actions";
import { axiosInstance } from "../client";

export interface AuthServiceState {
    isAuth: boolean;
    email: string;
    userName: string;

    isLoading: boolean;
    error?: string;
}

const initialState: AuthServiceState = {
    isAuth: localStorage.getItem("access_token") !== null,
    email: "",
    userName: "",

    isLoading: false,
    error: undefined,
} as AuthServiceState;

export function setToken(access_token: string, refresh_token?: string) {
    localStorage.setItem("access_token", access_token);
    axiosInstance.defaults.headers.common["Authorization"] = "Bearer " + access_token;
    if (refresh_token !== undefined) {
        localStorage.setItem("refresh_token", refresh_token);
    }
}

export function AuthServiceReducer(
    state = initialState,
    action: AuthServiceActionType
): AuthServiceState {
    switch (action.type) {
        case "register_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "register_SUCCESS":
            setToken(action.payload.access_token, action.payload.refresh_token);
            return {
                ...state,
                isAuth: true,
                userName: action.payload.username,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "register_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        case "login_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "login_SUCCESS":
            setToken(action.payload.access_token, action.payload.refresh_token);
            return {
                ...state,
                isAuth: true,
                userName: action.payload.username,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "login_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        case "refreshToken_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as AuthServiceState;

        case "refreshToken_SUCCESS":
            setToken(action.payload.access_token);
            return {
                ...state,
                isAuth: true,
                isLoading: false,
                error: undefined,
            } as AuthServiceState;

        case "refreshToken_REJECTED":
            return {
                ...state,
                isAuth: false,
                isLoading: false,
                error: action.payload,
            } as AuthServiceState;

        default:
            return state;
    }
}

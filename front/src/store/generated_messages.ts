// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


export interface RegisterRequest {
    email: string
    username: string
    password: string
}
export function construct_RegisterRequest(x: any): RegisterRequest {
    return x as RegisterRequest
}


export interface LoginRequest {
    email: string
    password: string
}
export function construct_LoginRequest(x: any): LoginRequest {
    return x as LoginRequest
}


export interface AuthResponse {
    user_id: number
    username: string
    access_token: string
    refresh_token: string
}
export function construct_AuthResponse(x: any): AuthResponse {
    return x as AuthResponse
}


export interface Token {
    access_token: string
}
export function construct_Token(x: any): Token {
    return x as Token
}


export interface Task {
    task_id: number
    project_id: number
    project_section: string
    title: string
    description: string
    priority: number
    utc_deadline_dttm: number
    utc_assigned_dttm: number
    parent_task_id?: number
    subtask_id_list: number[]
    is_deleted: boolean
    is_completed: boolean
}
export function construct_Task(x: any): Task {
    return x as Task
}


export interface NewTask {
    title: string
    description: string
    parent_task_id?: number
    project_id?: number
    project_section?: string
    priority?: number
    utc_deadline_dttm?: number
    utc_assigned_dttm?: number
}
export function construct_NewTask(x: any): NewTask {
    return x as NewTask
}


export interface TaskID {
    task_id: number
}
export function construct_TaskID(x: any): TaskID {
    return x as TaskID
}


export interface TaskList {
    list: Task[]
}
export function construct_TaskList(x: any): TaskList {
    return {
        list: x['list'].map((item: any) => construct_Task(item)),
    } as TaskList
}


export interface Project {
    project_id: number
    title: string
    color: string
    section_list: string[]
    is_shared: boolean
    user_id_list: number[]
    is_archived: boolean
    is_deleted: boolean
}
export function construct_Project(x: any): Project {
    return x as Project
}


export interface ProjectList {
    list: Project[]
}
export function construct_ProjectList(x: any): ProjectList {
    return {
        list: x['list'].map((item: any) => construct_Project(item)),
    } as ProjectList
}


export interface NewProject {
    title: string
    color: string
}
export function construct_NewProject(x: any): NewProject {
    return x as NewProject
}


export interface ProjectID {
    project_id: number
}
export function construct_ProjectID(x: any): ProjectID {
    return x as ProjectID
}


export interface Activity {
    user_id: number
    task_id: number
    change: string
}
export function construct_Activity(x: any): Activity {
    return x as Activity
}


export interface ActivityList {
    list: Activity[]
}
export function construct_ActivityList(x: any): ActivityList {
    return {
        list: x['list'].map((item: any) => construct_Activity(item)),
    } as ActivityList
}



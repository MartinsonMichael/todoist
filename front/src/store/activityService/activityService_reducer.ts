import * as msg from "../generated_messages"
import { ActivityServiceActionType } from "./activityService_actions"


export interface ActivityServiceState {
    // TODO add valuable state, probably rename

    isLoading: boolean
    error?: string
}

const initialState: ActivityServiceState = {
    // TODO add valuable state, probably rename

    isLoading: false,
    error: undefined,
} as ActivityServiceState;


export function ActivityServiceReducer(state = initialState, action: ActivityServiceActionType): ActivityServiceState {
    switch (action.type) {
        case "getChangeLog_START":
            return {
                ...state,
                isLoading: true,
                error: undefined,
            } as ActivityServiceState;

        case "getChangeLog_SUCCESS":
            return {
                ...state,
                isLoading: false,
                error: undefined,
            } as ActivityServiceState;

        case "getChangeLog_REJECTED":
            return {
                ...state,
                isLoading: false,
                error: action.payload,
            } as ActivityServiceState;


        default:
            return state
    }
}

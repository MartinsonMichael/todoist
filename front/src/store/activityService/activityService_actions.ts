// This file is generated, DO NOT EDIT IT
// Michael Martinson http generator (c)


import { axiosInstance } from "../client"
import * as msg from "../generated_messages"
import { AxiosResponse } from "axios";




export const getChangeLog_START = "getChangeLog_START";
interface getChangeLog_START_Action {
    type: typeof getChangeLog_START
    payload: undefined
}
export const getChangeLog_SUCCESS = "getChangeLog_SUCCESS";
interface getChangeLog_SUCCESS_Action {
    type: typeof getChangeLog_SUCCESS
    payload: msg.ActivityList
}
export const getChangeLog_REJECTED = "getChangeLog_REJECTED";
interface getChangeLog_REJECTED_Action {
    type: typeof getChangeLog_REJECTED
    payload: string
}

export const getChangeLog = () => {
    return async (dispatch: any) => {
        dispatch({type: getChangeLog_START, payload: undefined});

        await axiosInstance.post(
            'getChangeLog',
            {
                'headers': {
                    'Access-Control-Allow-Origin': '*',
                    'Access-Control-Allow-Headers': '*',
                },
            },
        ).then(
            (response: AxiosResponse<msg.ActivityList, any>) => {
                if (!Object.keys(response.headers).includes("error")) {
                    dispatch({type: getChangeLog_SUCCESS, payload: msg.construct_ActivityList(response.data)});
                } else {
                    dispatch({type: getChangeLog_REJECTED, payload: response.data});
                }
            }
        ).catch(
            (error: any) => {
                dispatch({type: getChangeLog_REJECTED, payload: error.toString()});
            }
        );
    }
};



export type ActivityServiceActionType = (
    getChangeLog_START_Action |
    getChangeLog_SUCCESS_Action |
    getChangeLog_REJECTED_Action 
)
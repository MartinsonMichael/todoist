import React, { useState } from "react";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import { BsCheckCircle, BsCircle } from "react-icons/bs";

const mapStoreStateToProps = (store: RootState) => ({
    taskList: store.task.taskList,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type TaskMarkerProps = ConnectedProps<typeof connector> & {
    task: msg.Task,
};


const TaskMarker: React.FC<TaskMarkerProps> = (props: TaskMarkerProps): React.ReactElement => {
    const [ hovered, setHover ] = useState(false);

    const color: string = props.task.is_completed ? "grey" : "black";
    return (
        <div
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            className="w-auto me-4"
        >
        {
            props.task.is_completed || hovered
                ?
                <BsCheckCircle color={color}/>
                :
                <BsCircle color={color}/>
        }
        </div>
    );
};

export default connector(TaskMarker);

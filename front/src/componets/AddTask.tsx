import React, {useState} from "react";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import {Col, Row, Button, Stack, Card, FormControl} from "react-bootstrap";
import { BsPlus, BsFillPlusCircleFill } from "react-icons/bs";

const mapStoreStateToProps = (store: RootState) => ({
    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type AddTaskProps = ConnectedProps<typeof connector> & {
    project_id?: number,
    project_section: string,
};


const AddTask: React.FC<AddTaskProps> = (props: AddTaskProps): React.ReactElement => {
    const [creating, setCreating] = useState(false);
    const [plusHovered, setplusHovered] = useState(false);

    if (!creating) {
        return (
            <Stack
                direction="horizontal"
                onMouseEnter={() => setplusHovered(true)}
                onMouseLeave={() => setplusHovered(false)}
                onClick={() => setCreating(true)}
            >
                { plusHovered ?
                    <BsFillPlusCircleFill color="red" className="me-2"/>
                    : <BsPlus color="red" className="me-2"/>
                }
                Add task
            </Stack>
        )
    }

    return (
        <Card
            className="ms-5 ps-0 pe-0"
        >

        </Card>
    );
};

export default connector(AddTask);

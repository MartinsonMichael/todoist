import React, {useState} from "react";
import { useNavigate} from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import {Col, Row, Button, NavLink, Nav, Stack} from "react-bootstrap";
import { BsThreeDots, BsFillCircleFill } from "react-icons/bs";
import ProjectSettings from "./ProjectSettings";
import assert from "assert";

const mapStoreStateToProps = (store: RootState) => ({
    taskList: store.task.taskList,
    inbox_project_id: store.project.inbox_project_id,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type ProjectTabProps = ConnectedProps<typeof connector> & {
    mode: "inbox" | "upcoming" | "today" | "project",
    title: string,
    icon?: React.ReactElement,
    project?: msg.Project,
};


const ProjectTab: React.FC<ProjectTabProps> = (props: ProjectTabProps): React.ReactElement => {
    assert(
        props.mode !== "project" || (props.mode === "project" && props.project !== undefined),
        "For mode === 'project', project must be defined"
    );
    const project: msg.Project = props.project || {project_id: -2} as msg.Project;

    const [ hovered, setHover ] = useState(false);
    const [ menuHovered, setMenuHover ] = useState(false);
    const [ settingsShow, settingShowChange ] = useState(false);

    const uts_today = new Date().getTime(); // check it!
    const navigate = useNavigate();

    const taskCount: number = (props.taskList || []).filter(
        (task: msg.Task) => (
            task.project_id === project.project_id ||
            (task.project_id === props.inbox_project_id && props.mode === "inbox") ||
            (task.utc_assigned_dttm === uts_today && props.mode === "today")
        )
    ).length;

    const link: string = (
        props.mode === "project" ? `/${project.project_id}`
        :
        `/${props.mode}`
    );

    return (
        <Stack
            direction="horizontal"
            gap={2}
            className="align-items-center pe-2 ps-2"
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            style={{backgroundColor: (hovered ? "lightgray" : "white")}}
        >
            { props.icon !== undefined ? props.icon : <BsFillCircleFill className="w-auth" style={{color: project.color}}/> }
            <NavLink onClick={() => navigate(link)} style={{color: "black"}} className="ps-0 pe-0">
                {props.title}
            </NavLink>
            <div className="text-secondary ms-auto" />
            { hovered && props.mode === "project" ?
                <BsThreeDots
                    color={menuHovered ? "black" : "gray"}
                    onClick={() => settingShowChange(true)}
                    onMouseEnter={() => setMenuHover(true)}
                    onMouseLeave={() => setMenuHover(false)}
                />
                :
                taskCount
            }
            {props.mode === "project" && props.project !== undefined?
                <ProjectSettings
                    mode="update"
                    project={ props.project }
                    show={ settingsShow }
                    onClose={ () => settingShowChange(false) }
                />
                : null
            }
        </Stack>
    );
};

export default connector(ProjectTab);

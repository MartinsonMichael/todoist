import React, {useState} from "react";
import { connect, ConnectedProps } from "react-redux";
import { updateProject, createProject } from "../store/projectService/projectService_actions"

import {ColorResult, SwatchesPicker} from 'react-color';

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import { Button, Modal, InputGroup, FormControl } from "react-bootstrap";
import { BsFillCircleFill } from "react-icons/bs";
import assert from "assert";

const mapStoreStateToProps = (store: RootState) => ({
    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
        updateProjectSettings: (title: string, color: string, p: msg.Project) => dispatch(
            updateProject(p.project_id, title, color, p.section_list, p.is_shared, p.user_id_list, p.is_archived, p.is_deleted)
        ),
        createProject: (title: string, color: string) => dispatch(
            createProject(title, color)
        ),
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type ProjectSettingsProps = ConnectedProps<typeof connector> & {
    mode: "update" | "create",
    show: boolean,
    onClose: () => void,
    project?: msg.Project,
};

const ProjectSettings: React.FC<ProjectSettingsProps> = (props: ProjectSettingsProps): React.ReactElement => {

    assert(
        props.mode === "create" || (props.mode === "update" && props.project !== undefined),
        "for more 'update' project should be provided"
    );

    const [ title, setTitle ] = useState(props.project ? props.project.title : "");
    const [ color, setColor ] = useState(props.project ? props.project.color : "");

    function onClick(title: string, color: string, project?: msg.Project) {
        if (props.mode === "update" && project !== undefined) {
            props.updateProjectSettings(title, color, project);
        } else if (props.mode === "create") {
            props.createProject(title, color);
        }
        props.onClose();
    }

    return (
        <Modal show={ props.show } onHide={props.onClose} >
            <Modal.Header>
                { props.mode === "update" ? "Project Settings" : "Create new project" }
            </Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-3">
                    <InputGroup.Text>Title</InputGroup.Text>
                    <FormControl
                        value={ title }
                        onChange={e => setTitle(e.target.value)}
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <InputGroup.Text>Color</InputGroup.Text>
                    <InputGroup.Text>
                        <BsFillCircleFill style={{color: color}}/>
                    </InputGroup.Text>
                </InputGroup>
                <SwatchesPicker
                    color={ color }
                    onChange={(c: ColorResult, e: any) => setColor(c.hex)}
                />
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={props.onClose}>Cancel</Button>
                <Button onClick={() => onClick(title, color, props.project)}>
                    { props.mode === "update" ? "Update" : "Create" }
                </Button>
            </Modal.Footer>
        </Modal>
    );
};

export default connector(ProjectSettings);

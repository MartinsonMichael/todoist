import React from "react";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import { Stack } from "react-bootstrap";
import TaskCard from "./TaskCard";
import AddTask from "./AddTask";

const mapStoreStateToProps = (store: RootState) => ({
    projectList: store.project.projectList,
    taskList: store.task.taskList,

    inbox_project_id: store.project.inbox_project_id,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type TaskViewerProps = ConnectedProps<typeof connector> & {};

const TodayTaskViewer: React.FC<TaskViewerProps> = (props: TaskViewerProps): React.ReactElement => {
    const taskFilter = (task: msg.Task): boolean => {
        return false;//task.project_id === props.inbox_project_id;
    };
    const task_list: msg.Task[] = (props.taskList || []).filter(taskFilter);

    return (
        <Stack direction="vertical" className="mt-3">
            <div className="fw-bold mb-3"> Today </div>
            { task_list.map((task: msg.Task) => (
                <TaskCard
                    key={task.task_id}
                    task={ task }
                />
            ))}
            {/*<AddTask project_id={props.inbox_project_id} project_section=""/>*/}
        </Stack>
    );
};

export default connector(TodayTaskViewer);

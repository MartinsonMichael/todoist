import * as React from "react";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import {Modal, InputGroup, Row, FormControl, Button, Spinner} from "react-bootstrap";
import { BsFillEyeFill, BsFillEyeSlashFill } from "react-icons/bs";
import { register, login, refreshToken } from "../store/authService/authService_actions"

const mapStoreStateToProps = (store: RootState) => ({
    isAuth: store.auth.isAuth,
    userName: store.auth.userName,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});

const mapDispatchToProps = (dispatch: any) => {
    return {
        updateToken: () => dispatch(refreshToken()),

        registerNewUser: (email: string, password: string, userName: string) => dispatch(
            register(email, userName, password)
        ),
        loginUser: (email: string, password: string) => dispatch(login(email, password)),
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);
type PropsFromRedux = ConnectedProps<typeof connector>;

export interface RegisterState {
    show: boolean;
    active: "login" | "register";

    password: string;
    showPassword: boolean;

    userName: string;
    email: string;
}

export type RegisterProps = PropsFromRedux & {};

class Register extends React.Component<RegisterProps, RegisterState> {
    constructor(props: RegisterProps) {
        super(props);
        this.state = {
            show: localStorage.getItem("token") === null,
            active: "register",

            password: "",
            showPassword: false,

            userName: "",
            email: "",
        };
        this.renderLoginInput = this.renderLoginInput.bind(this);
        this.renderEmailPasswordInput = this.renderEmailPasswordInput.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount(): void {
        this.props.updateToken();
    }

    componentDidUpdate(prevProps: Readonly<RegisterProps>, prevState: Readonly<RegisterState>, snapshot?: any): void {
        if (!prevProps.isAuth && this.props.isAuth) {
            this.onClose();
        }
        if (!prevState.show && !this.props.isAuth) {
            this.setState({show: true});
        }
    }

    onClose(): void {
        this.setState({show: false});
    }

    onLogin(): void {
        this.props.loginUser(this.state.email, this.state.password);
    }

    onRegister(): void {
        this.props.registerNewUser(this.state.email, this.state.password, this.state.userName);
    }

    renderLoginInput(): React.ReactNode {
        const { userName } = this.state;
        return (
            <InputGroup className="mb-2">
                <InputGroup.Text>Login</InputGroup.Text>
                <FormControl
                    value={userName}
                    placeholder="How should we call you?..."
                    onChange={(e) => this.setState({ userName: e.target.value })}
                />
            </InputGroup>
        )
    }

    renderEmailPasswordInput(): React.ReactNode {
        const { showPassword, password, email } = this.state;
        return (
            <>
            <InputGroup className="mb-2">
                <InputGroup.Text>Email</InputGroup.Text>
                <FormControl
                    type="email"
                    value={email}
                    placeholder="your email..."
                    onChange={(e) => this.setState({ email: e.target.value })}
                />
            </InputGroup>
            <InputGroup className="mb-2">
                <InputGroup.Text>Password</InputGroup.Text>
                <FormControl
                    type={showPassword ? "text" : "password"}
                    value={password}
                    placeholder="any combination of symbols..."
                    onChange={(e) => this.setState({ password: e.target.value })}
                />
                <Button
                    variant="outline-primary"
                    onClick={() => this.setState({ showPassword: !this.state.showPassword })}
                >
                    {showPassword ? <BsFillEyeFill /> : <BsFillEyeSlashFill />}
                </Button>
            </InputGroup>
            </>
        )
    }

    render(): React.ReactNode {
        const { show, active } = this.state;
        const { isLoading } = this.props;

        return (
            <Modal show={show && !this.props.isAuth} size="lg">
                <Modal.Header>
                    {this.props.isLoading ? <Spinner animation="border"/> : null}
                    {this.props.error !== undefined ? <p>{this.props.error}</p> : null}
                </Modal.Header>
                <Modal.Body>
                    <Row className="mb-2 me-0 ms-0">
                        <Button
                            variant={active === "login" ? "outlined-secondary" : "primary"}
                            disabled={ active === "login" }
                            className="w-auto me-2"
                            onClick={() => this.setState({active: "login"})}
                        >
                            Login
                        </Button>
                        <Button
                            className="w-auto me-2"
                            variant={active === "register" ? "outlined-secondary" : "primary"}
                            disabled={ active === "register" }
                            onClick={() => this.setState({active: "register"})}
                        >
                            Register
                        </Button>
                    </Row>
                    { active === "register" ? this.renderLoginInput() : null }
                    { this.renderEmailPasswordInput() }
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        variant="primary"
                        disabled={ (this.state.userName === "" && active === "register") || this.state.password === "" || this.state.email === "" }
                        onClick={() => {
                            if (active === "login") {
                                this.onLogin();
                            } else {
                                this.onRegister();
                            }
                        }}

                    >
                        {active === "register" ? "Register" : "Login"}
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default connector(Register);

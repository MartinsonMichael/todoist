import React, {useState} from "react";
import { useParams } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import { Stack } from "react-bootstrap";
import TaskMarker from "./TaskMarker";

const mapStoreStateToProps = (store: RootState) => ({
    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type TaskCardProps = ConnectedProps<typeof connector> & {
    task: msg.Task,
};

const TaskCard: React.FC<TaskCardProps> = (props: TaskCardProps): React.ReactElement => {

    const [ hovered, setHover ] = useState(false);

    return (
        <Stack
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            className="mb-2"
            direction="horizontal"
        >
            <TaskMarker task={props.task}/>
            <Stack direction="vertical">
                <div>{ props.task.title }</div>
                <div style={{color: "lightgray", fontSize: 14}}>{ props.task.description.slice(0, 124) }</div>
            </Stack>
            <hr style={{color: "black"}}/>
        </Stack>
    );
};

export default connector(TaskCard);

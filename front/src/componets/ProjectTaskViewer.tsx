import React from "react";
import { useParams } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import { Stack } from "react-bootstrap";
import TaskCard from "./TaskCard";
import AddTask from "./AddTask";

const mapStoreStateToProps = (store: RootState) => ({
    projectList: store.project.projectList,
    taskList: store.task.taskList,

    inbox_project_id: store.project.inbox_project_id,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {
    };
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type TaskViewerProps = ConnectedProps<typeof connector> & {};

const ProjectTaskViewer: React.FC<TaskViewerProps> = (props: TaskViewerProps): React.ReactElement => {

    const pathParams = useParams();
    const project_id: number = Number(pathParams.project_id);
    const project: msg.Project = (
        (props.projectList || [])
        .filter((p: msg.Project) => p.project_id === project_id)
    )[0];

    const taskFilter = (task: msg.Task): boolean => {
        return task.project_id === project_id;
    };
    const task_list: msg.Task[] = (props.taskList || []).filter(taskFilter);

    return (
        <Stack direction="vertical" className="mt-3">
            <div className="fw-bold mb-3">{ project.title }</div>
            { task_list
                .filter((task: msg.Task) => task.project_section === "" || task.project_section === undefined || task.project_section === null)
                .map((task: msg.Task) => (
                    <TaskCard
                        key={task.task_id}
                        task={ task }
                    />
                )
            )}
            {/*<AddTask project_id={project.project_id} project_section=""/>*/}
            { project.section_list.map((sectionTitle: string) => (
                    <Stack direction="vertical" key={sectionTitle}>
                        <div className="fw-bold mb-3">{ sectionTitle }</div>
                        { task_list
                            .filter((task: msg.Task) => task.project_section === sectionTitle)
                            .map((task: msg.Task) => (
                                <TaskCard
                                    task={ task }
                                />
                            )
                        )}
                        {/*{ !is_special ?*/}
                        {/*    <AddTask project_id={project.project_id} project_section={sectionTitle}/>*/}
                        {/*    : null*/}
                        {/*}*/}
                    </Stack>
                )
            )}
        </Stack>
    );
};

export default connector(ProjectTaskViewer);

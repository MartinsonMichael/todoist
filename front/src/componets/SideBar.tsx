import React, {useState} from "react";
import { connect, ConnectedProps } from "react-redux";

import { RootState } from "../store";
import * as msg from "../store/generated_messages";
import {Col, Row, Stack} from "react-bootstrap";
import {BsFillInboxFill, BsCalendarDate, BsCalendarWeek, BsPlus, BsFillPlusCircleFill} from "react-icons/bs";
import ProjectTab from "./ProjectTab";
import ProjectSettings from "./ProjectSettings";

const mapStoreStateToProps = (store: RootState) => ({
    projectList: store.project.projectList,
    taskList: store.task.taskList,

    isLoading: store.auth.isLoading,
    error: store.auth.error,
});
const mapDispatchToProps = (dispatch: any) => {
    return {};
};
const connector = connect(mapStoreStateToProps, mapDispatchToProps);

type SideBarProps = ConnectedProps<typeof connector> & {};

function getInboxCount(taskList?: msg.Task[]): string {
    if (taskList === undefined) {
        return "?"
    }
    return taskList.filter((task: msg.Task) => task.project_id === undefined).length.toString()
}

const SideBar: React.FC<SideBarProps> = (props: SideBarProps): React.ReactElement => {
    const projectList: msg.Project[] = props.projectList || [];

    const [ hovered, setHover] = useState(false);
    const [ addProjectHovered, addProjectsetHovered] = useState(false);

    const [ newProject, showNewProject ] = useState(false);

    return (
        <Stack
            className="ms-5 ps-0 pe-0 justify-content-start"
            direction="vertical"
            style={{width: "305px"}}
            onMouseEnter={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
        >
            <div className="mb-5"/>
            <ProjectTab
                mode="inbox"
                title="Inbox"
                icon={<BsFillInboxFill className="w-auto" style={{"color": "blue"}}/>}
            />
            <ProjectTab
                mode="today"
                title="Today"
                icon={<BsCalendarDate className="w-auto" style={{"color": "green"}}/>}
            />
            <ProjectTab
                mode="upcoming"
                title="Upcoming"
                icon={<BsCalendarWeek className="w-auto" style={{"color": "purple"}}/>}
            />
            <div className="mb-5"/>
            <Stack
                className="align-items-center mb-3"
                direction="horizontal"
            >
                <div className="fw-bold">Projects</div>
                <div className="flex-grow-1"/>
                { hovered ?
                    (addProjectHovered ?
                        <BsFillPlusCircleFill
                            color="red"
                            className="w-auto pe-1"
                            onMouseEnter={() => addProjectsetHovered(true)}
                            onMouseLeave={() => addProjectsetHovered(false)}
                            onClick={() => showNewProject(true)}
                        />
                        :
                        <BsPlus
                            color="red"
                            className="w-auto pe-1"
                            onMouseEnter={() => addProjectsetHovered(true)}
                            onMouseLeave={() => addProjectsetHovered(false)}
                            onClick={() => showNewProject(true)}
                        />
                    )
                    : null
                }
            </Stack>
            { projectList
                .filter((project: msg.Project) => project.title !== "Inbox")
                .map((project: msg.Project) => (
                    <ProjectTab
                        mode="project"
                        key={ project.project_id }
                        title={ project.title }
                        project={ project }
                    />
                )
            ) }
            <ProjectSettings
                mode="create"
                show={ newProject }
                onClose={() => showNewProject(false)}
            />
        </Stack>
    );
};

export default connector(SideBar);

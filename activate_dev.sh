#!/bin/bash

cd back

if [ ! -d pyenv ]; then
  echo "Installing virtual env..."
  python3.8 -m venv pyenv
fi;

echo "Installing packages..."
source pyenv/bin/activate
pip install -r requirements.txt

echo "Making test folders..."
cd ..
mkdir db_files
mkdir db_files_test

echo "Done"